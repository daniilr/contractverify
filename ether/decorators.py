from inspect import isclass
from functools import WRAPPER_ASSIGNMENTS

from django.contrib.auth.decorators import login_required

def expand_to_class(deco):
    def new_deco(obj):
        if isclass(obj):
            class Wrap(obj):
                def dispatch(self, *args, **kwargs):
                    return deco(super(Wrap, self).dispatch)(*args, **kwargs)

            Wrap.__name__ = obj.__name__
            return Wrap
        else:
            return deco(obj)
    return new_deco

login_required = expand_to_class(login_required)