import json
import uuid

from django.contrib.auth.models import User
from django.db.models import Model, ForeignKey, OneToOneField, CharField, \
    TextField, UUIDField, DateField, IntegerField, BooleanField, ManyToManyField, DateTimeField
from django.contrib.postgres.fields import JSONField
from django.db.models.fields.files import FileField
from django.utils.translation import ugettext as _
from django_countries.fields import CountryField


class Organization(Model):
    uuid    = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user    = OneToOneField(User)
    name    = TextField()
    country = CountryField()
    data    = JSONField()

    def __str__(self):
        return '{}, "{}"'.format(self.data_obj()['INN'], self.name)

    def data_obj(self):
        return json.loads(self.data)


class Document(Model):
    title        = TextField()
    name         = TextField()
    nomenclature = TextField()
    sign_date    = DateField()
    file         = FileField(null=True, blank=True)

    def __str__(self):
        return self.title


class Transaction(Model):
    ACCEPTED = 1
    PENDING = 2
    DISCARDED = 3

    STATUS_CHOICES = {
        ACCEPTED: _('Accepted'),
        PENDING: _('Pending'),
        DISCARDED: _('Discarded'),
    }.items()

    status  = IntegerField(choices=STATUS_CHOICES)
    creator = ForeignKey(Organization, related_name='created_transactions')
    orgs    = ManyToManyField(Organization, related_name='all_transactions')
    doc     = OneToOneField(Document)

    approvements = ManyToManyField(Organization,
                                   through='Approvement')

    contractAddress = TextField(blank=True)

    date = DateField(auto_now_add=True)

    def __str__(self):
        return str(self.pk)


class Approvement(Model):
    ACCEPT = 1
    DECLINE = 2

    ACTION_CHOICES = {
        ACCEPT: _('accept'),
        DECLINE: _('decline'),
    }.items()

    transaction  = ForeignKey(Transaction)
    organization = ForeignKey(Organization)
    action       = IntegerField(choices=ACTION_CHOICES)
    comment      = TextField()
    date         = DateTimeField(auto_now_add=True)
