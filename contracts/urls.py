from django.conf.urls import url

from contracts.views import CreateTransactionView, ApproveView

urlpatterns = [
    url('^create$', CreateTransactionView.as_view(), name='create'),
    url('^approve$', ApproveView.as_view(), name='approve'),
]