from django.contrib import admin

from contracts.models import Transaction, Document, Organization, Approvement

admin.site.register(Organization)
admin.site.register(Transaction)
admin.site.register(Document)
admin.site.register(Approvement)
