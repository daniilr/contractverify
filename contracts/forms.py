from django.forms.models import modelform_factory
from django.forms.widgets import TextInput
from django.utils.translation import ugettext as _

from contracts.models import Document

DocumentForm = modelform_factory(Document,
                                 fields=['name',
                                         'nomenclature',
                                         'title',
                                         'file',
                                         'sign_date', ],
                                 labels={'name': _('Document type name'),
                                         'title': _('Document title'),
                                         'nomenclature': _('Document number')},
                                 widgets={'name': TextInput,
                                          'nomenclature': TextInput,})
