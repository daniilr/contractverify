from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Prefetch
from django.http.response import HttpResponseRedirect
from django.views.generic import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from contracts.forms import DocumentForm
from contracts.models import Organization, Transaction, Approvement
from ether.decorators import login_required


class ApprovementChoice(object):
    ACCEPT  = 'accept'
    DECLINE = 'decline'

    actions = {ACCEPT: Approvement.ACCEPT,
               DECLINE: Approvement.DECLINE,}

@login_required
class MainView(ListView):
    template_name = 'contracts/main.html'
    model = Transaction
    ordering = 'date'

    def get_context_data(self, **kwargs):
        kwargs['ApprovementChoice'] = ApprovementChoice
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return Transaction.objects.\
            prefetch_related('orgs__user',
                             Prefetch('approvements',
                                      queryset=Approvement.objects.all())).\
            select_related('doc', 'creator__user').\
            order_by('-date')


@login_required
def my_profile(request):
    return HttpResponseRedirect(
        reverse('profile', kwargs={'pk': request.user.organization.pk}))


class ProfileView(DetailView):
    template_name = 'contracts/profile.html'
    context_object_name = 'p'
    model = Organization


@login_required
class CreateTransactionView(CreateView):
    template_name = 'contracts/transaction/create.html'
    success_url = reverse_lazy('main')
    model = Transaction
    fields = ['orgs']

    def get_context_data(self, **kwargs):
        kwargs['doc_form'] = self.get_doc_form()
        return super().get_context_data(**kwargs)

    def _get_doc_form_kwargs(self):
        kwargs = {}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def get_doc_form(self):
        return DocumentForm(**self._get_doc_form_kwargs())

    def form_valid(self, form, doc_form=None):
        doc = doc_form.save()
        transaction = form.save(commit=False)
        transaction.doc = doc
        transaction.status = Transaction.PENDING
        transaction.creator = self.request.user.organization
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        doc_form = self.get_doc_form()
        self.object = None
        if form.is_valid() and doc_form.is_valid():
            return self.form_valid(form, doc_form)
        else:
            return self.form_invalid(form)


@login_required
class ApproveView(View):
    def redirect_to_main(self):
        return HttpResponseRedirect(reverse('main'))

    def post(self, request):
        transaction = Transaction.objects.filter(
            pk=request.POST.get('transaction')).first()
        if not transaction:
            return self.redirect_to_main()
        if not transaction.orgs.filter(user=request.user).exists():
            return self.redirect_to_main()

        for action, approvement in ApprovementChoice.actions.items():
            if action in self.request.POST:
                Approvement.objects.get_or_create(
                    transaction=transaction,
                    organization=request.user.organization,
                    action=ApprovementChoice.actions[action])
                break
        return self.redirect_to_main()






