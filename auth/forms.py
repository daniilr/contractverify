import itertools
import json

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db import transaction
from django.forms import CharField
from django.forms.fields import CallableChoiceIterator
from django.utils.translation import ugettext as _
from django_countries import countries
from django_countries.fields import LazyTypedChoiceField

from contracts.models import Organization

COUNTRY_CHOICES = lambda: itertools.chain((('', _('<Select country>')),),
                                          countries)

class RegistrationFrom(UserCreationForm):
    name = CharField()
    country = LazyTypedChoiceField(
        choices=CallableChoiceIterator(COUNTRY_CHOICES))

    INN = CharField()

    class Meta:
        fields = ('email',)
        model = User

    def save(self, commit=True):
        user = super(RegistrationFrom, self).save(commit=False)
        user.username = user.email

        if commit:
            org = Organization(name=self.cleaned_data['name'],
                               country=self.cleaned_data['country'],
                               data=json.dumps({
                                   'INN': self.cleaned_data['INN']}))
            with transaction.atomic():
                user.save()
                org.user = user
                org.save()
        return user
