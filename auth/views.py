from functools import partial

from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth import login as auth_login

from auth.forms import RegistrationFrom

MAIN = reverse_lazy('main')

login = partial(auth_views.login,
                template_name='auth/login.html',
                redirect_field_name=MAIN)

logout = partial(auth_views.logout,
                 next_page=MAIN)

class RegistrationView(CreateView):
    model = User
    form_class = RegistrationFrom
    success_url = MAIN
    template_name = 'auth/registration.html'

    def form_valid(self, form):
        response = super(RegistrationView, self).form_valid(form)
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        auth_login(self.request, self.object)
        return response